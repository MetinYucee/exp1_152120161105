﻿#include<iostream>
#include<fstream>   // text dosyasını okumayı sağlayan kütüphane.
#include "MyClass1.h"
using namespace std;

int sum(int array[])
{
	int sum = 0;
	for (int i = 0; i < 4; i++)   // toplama işlemi için fonksiyon.
	{
		sum += array[i];

	}
	cout << "sum is " << sum<<endl;

	return sum;
}

int product(int array[])            // çarpma işlemi için fonksiyon.
{
	int product = 1;
	for (int i = 0; i < 4; i++)
	{
		product *= array[i];

	}
	cout << "product is " << product<<endl;

	return product;




}

int average(int array[])              // ortalamayı bulmak için fonksiyon.
{
	int average = 0;
	int sum = 0;
	for (int i = 0; i < 4; i++)
	{
		sum += array[i];

	}
	average = sum / 4;
	cout << "average is " << average<<endl;

	return average;



}

int smallest(int array[])      // en küçük sayıyı bulmak için fonksiyon.
{
	int smallest = array[0];
	int degisken=0;

	for (int i = 1; i < 4; i++) 
	{
		if (array[i] <smallest) 
		{
			smallest = array[i];
			

		}


	}

	cout << "smallest is " << smallest;

	return smallest;
}
int main()
{

	int array[4];                    // text dosyasındaki sayıları aktarmak için oluşturduğum dizi.
	ifstream giris_dosyasi;             
	giris_dosyasi.open("sayılar.txt");     // text dosyasını açmaya yarayan komut.
	for (int i = 0; i < 4; i++)            // text dosyasındaki değerleri kurulan arrayin içine atmak için for döngüsü.
	{
		giris_dosyasi >> array[i];
		/*cout << array[i] << " ";*/ // dizideki sayıları yazdırmak için.
	}
	/*cout << endl;*/
	sum(array);                  //  her bir işlem için gereken fonskiyon çağırmalar.
	product(array);
	average(array);
	smallest(array);

	giris_dosyasi.close();   // açılan text dosyasını kapatmaya yarayan komut.
	return 0;
}
